# Service Worker Playground

A playground for exploring javascript service worker features. Consists of a small app `app.js` for loading images on the screen used by different scenarios. 

## Scenarios

### No-caching

In this example no service worker is loaded and no cashing is performed what so ever, not even what would normally be done by the browser automatically so as to compare with other caching strategies.

### Cache falling back to network

In this example the requests are all handled either from cache or network. If the requested file is already cached, it will be served from the cache, or else, will make the request from the network. 

In this example a message is sent to the client that the fetch event has been triggered. 