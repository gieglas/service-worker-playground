//cache name
const cacheName = 'cache-fallback-network';

//assets to precache
const staticAssets = [
    'media/file_example_JPG_100kB.jpg',
    'media/file_example_JPG_500kB.jpg'
];

//install event
self.addEventListener('install', async event => {
    console.log("Install Event");
    //open cache
    const cache = await caches.open(cacheName); 
    //add all assets to be precached to the cache
    await cache.addAll(staticAssets); 
    console.log("Assets cached");
  });

//activate event
self.addEventListener('activate', event => {
    console.log("Activate Event");
    // delete any caches that aren't in expectedCaches    
    event.waitUntil(
      caches.keys().then(keys => Promise.all(
        keys.map(key => {
          if (!cacheName.includes(key)) {
            console.log("Delete previouse cache");
            return caches.delete(key);
          }
        })
      )).then(() => {
        console.log("New cache now ready.");
      })
    );
  });

//fetch event
self.addEventListener('fetch', async event => {
    console.log('Fetch event for: '+ event.request.url);
    event.respondWith(cacheFirst(event));
  });

/**
 * Handles the cache first strategy
 * 
 * @param {*} event The event objectS
 */
async function cacheFirst(event) {
  //open cache
  const cache = await caches.open(cacheName); 

  //get cached Response
  const cachedResponse = await cache.match(event.request);  
  
  if (cachedResponse) {
    console.log('Found in cache :-)');
    console.log(cachedResponse);
    console.log('Fetch event from cahce : ' + event.request.url);
    // Exit early if we don't have access to the client.
    // Eg, if it's cross-origin.
    if (!event.clientId) return null;
    // get the client from client id
    const client = await clients.get(event.clientId);
    send_message_to_client(client, 'Fetch event from cahce : '+ event.request.url, 50000);
  } else {
    console.log('NOT found in cache.');
  }
  //Return cached respponse or fetch from network
  return cachedResponse || fetch(event.request); 
}
    
/**
 * Sends a message to the client
 * 
 * @param {*} client The client making the triggered the event 
 * @param {*} msg The message to send to the client
 * @param {*} timeoutAfter Not used
 */
function send_message_to_client(client, msg, timeoutAfter){
  return new Promise(function(resolve, reject){
      var msg_chan = new MessageChannel();

      msg_chan.port1.onmessage = function(event){
          if(event.data.error){
              reject(event.data.error);
          }else{
              resolve(event.data);
          }
      };

      client.postMessage(msg, [msg_chan.port2]);

  });
}

/**
 * Sends a message to all the clients. 
 * Usefull i.e. when want to send an offline/online message
 * 
 * @param {*} msg The message to send to all the clients
 */
async function send_message_to_all_clients(msg){
  clients.matchAll().then(clients => {
      clients.forEach(client => {
          send_message_to_client(client, msg);
      })
  })
}

