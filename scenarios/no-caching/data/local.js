var locals = {
    "scenario" : "no-caching",
    "mediaTitle":"Clear",
    "mediaLinks" : {
        "navLink1" : {"id":"navLink1","url":"media/file_example_JPG_100kB.jpg","text":"100 kb Image"},
        "navLink2" : {"id":"navLink2","url":"media/file_example_JPG_500kB.jpg","text":"500 kb Image"}
      },
      "topBarTitle" : "Service Worker Playground",
      "topBarLinks" : {
        "link1" : {"id":"link1","url":"../../","text":"Home"}
      }
}