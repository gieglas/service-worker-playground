var locals = {
    "scenario" : "home",
    "topBarTitle" : "Service Worker Playground",
    "topBarLinks" : {
        "navLink1" : {"id":"navLink1","url":"scenarios/no-caching/","text":"No Caching"},
        "navLink2" : {"id":"navLink2","url":"scenarios/cache-fallback-network/","text":"Cache falling back to network"}
    }
}