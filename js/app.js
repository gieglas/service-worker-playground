/**
 * Funtion to register service worker
 * 
 * @param {String} scenario The selected scenario (to load the correct service worker)
 */
function registerSW(scenario) { 
    //only for scenarios that involve a service worker
    if ((scenario != 'home') && (scenario != 'no-caching')) {   
        if ('serviceWorker' in navigator) {
            try {
                //add message event listener
                navigator.serviceWorker.addEventListener('message', function(event) {                    
                    appendMessage("Message from Service Worker", event.data,event.timeStamp);
                    /*//If offline	       
                    if (event.data == "Offline") {
                        showGenericAlert('offline');
                    } else if (event.data == "Online") {
                        hideGenericAlert();
                    }*/
                });
                appendMessage('Register Service Worker', scenario + '.js',0);
                navigator.serviceWorker.register(scenario+'.js');
            } catch (e) {
                alert('ServiceWorker registration failed. Sorry about that.'); 
            }
        }
    } else {
        appendMessage('Message','NO Service Worker registered',0);
    }
}

/**
 * Appends a message on the `.messages` div
 * 
 * @param {String} msg Message to be appended on the screen
 */
function appendMessage(title,msg,id) {
    var toast = '<div id="'+String(id).replace(".", "")+'" class="toast" data-autohide="false">'+
    '<div class="toast-header">'+
      '<strong class="mr-auto text-primary">'+title+'</strong>'+
      '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>'+
    '</div>'+
    '<div class="toast-body">'+
      msg +
    '</div>'+
  '</div>';
    $( "#messages" ).append( toast);
    $('#'+String(id).replace(".", "")).toast('show');
}

/**
 * Highlight code using hightlight.js
 */
function highlightCode() {
    //highlight js
    $('pre code').each(function(i, e) {hljs.highlightBlock(e)}); 
}

/**
 * On window load
 */
$(window).on('load', function() {
    
    registerSW(locals.scenario);
    var mediaLinks = null;
    //initialize stuff and load data on shell
    $('#mainFooter').html(globals.footer);
    if (locals.mediaTitle) {
        $('#navBrand').html(locals.mediaTitle);
    }
    //fill media links toolbar
    if (locals.mediaLinks) {
        mediaLinks = locals.mediaLinks;
        var mainNavItems ="";
        $.each( mediaLinks, function( index, value ){
            mainNavItems += '<li class="nav-item"><button type="button" class="btn btn-link nav-link" id="'+value.id+'">'+value.text+'</button></li>';
        });
        $('#mainNavItems').html(mainNavItems);
    }
    
    //fill links for toolbar
    if (locals.topBarLinks) {
        var topNavItems ="";
        $.each( locals.topBarLinks, function( index, value ){
            topNavItems += '<li class="nav-item"><a class="nav-link" href="'+value.url
                +'"'+ ((value.blank)?" target=\"_blank\"":"")+ '>'+value.text+'</a></li>';
        });
        $('#topNavItems').html(topNavItems);
    }
    if (locals.topBarTitle) {
        $('#topBrand').html(locals.topBarTitle);
    }
    
    //----------Register Click Events ---------------------
    //click on any of the links
    $('#mainNav').on('click', '.nav-link', function(elem) {
        var id = this.id;
        if(!mediaLinks[id]["direct"]) {
            //create image object on dom
            $("#mainImage").html("<img class='img-fluid' id='img-"+id+"'>");
            //toggle active in navbar
            $("#mainNav").find(".active").removeClass("active");
            $(this).parent().addClass("active");
            //fetch image and load on page
            fetch(mediaLinks[id]["url"] + 
                (((locals.scenario == 'home') || (locals.scenario == 'no-caching'))?"?t="+Date.now():""))
                .then(function(response) {
                if(response.ok) {
                    response.blob().then(function(myBlob) {
                    var objectURL = URL.createObjectURL(myBlob);
                    var img = document.getElementById("img-"+id);
                    img.src = objectURL;
                    });
                } else {
                    console.log('Network response was not ok.');
                }
            }).catch(function(error) {
                console.log('There has been a problem with your fetch operation: ' + error.message);
            });
        } else {
            //If direct go to URL
            window.location.href = mediaLinks[id]["url"];
        }
    });
    //click on nav brand
    $('#mainNav').on('click', '.navbar-brand', function(elem) {        
        $("#mainImage").html("");
    });

    //Markdown with Pagedown		
    // create a pagedown converter - regular and sanitized versions are both supported
    var converter = new Markdown.getSanitizingConverter();
    // tell the converter to use Markdown Extra
    Markdown.Extra.init(converter, {highlighter: "highlight"});
    //get README.MD
    $.get('README.MD', function(textData, status) { 
        $("#mainText").html(converter.makeHtml(textData ));
        highlightCode(); 
    });
    
    
});
